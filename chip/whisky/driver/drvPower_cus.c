//<MStar Software>
//******************************************************************************
// MStar Software
// Copyright (c) 2010 - 2012 MStar Semiconductor, Inc. All rights reserved.
// All software, firmware and related documentation herein ("MStar Software") are
// intellectual property of MStar Semiconductor, Inc. ("MStar") and protected by
// law, including, but not limited to, copyright law and international treaties.
// Any use, modification, reproduction, retransmission, or republication of all 
// or part of MStar Software is expressly prohibited, unless prior written 
// permission has been granted by MStar. 
//
// By accessing, browsing and/or using MStar Software, you acknowledge that you
// have read, understood, and agree, to be bound by below terms ("Terms") and to
// comply with all applicable laws and regulations:
//
// 1. MStar shall retain any and all right, ownership and interest to MStar
//    Software and any modification/derivatives thereof.
//    No right, ownership, or interest to MStar Software and any
//    modification/derivatives thereof is transferred to you under Terms.
//
// 2. You understand that MStar Software might include, incorporate or be
//    supplied together with third party`s software and the use of MStar
//    Software may require additional licenses from third parties.  
//    Therefore, you hereby agree it is your sole responsibility to separately
//    obtain any and all third party right and license necessary for your use of
//    such third party`s software. 
//
// 3. MStar Software and any modification/derivatives thereof shall be deemed as
//    MStar`s confidential information and you agree to keep MStar`s 
//    confidential information in strictest confidence and not disclose to any
//    third party.  
//
// 4. MStar Software is provided on an "AS IS" basis without warranties of any
//    kind. Any warranties are hereby expressly disclaimed by MStar, including
//    without limitation, any warranties of merchantability, non-infringement of
//    intellectual property rights, fitness for a particular purpose, error free
//    and in conformity with any international standard.  You agree to waive any
//    claim against MStar for any loss, damage, cost or expense that you may
//    incur related to your use of MStar Software.
//    In no event shall MStar be liable for any direct, indirect, incidental or
//    consequential damages, including without limitation, lost of profit or
//    revenues, lost or damage of data, and unauthorized system use.
//    You agree that this Section 4 shall still apply without being affected
//    even if MStar Software has been modified by MStar in accordance with your
//    request or instruction for your use, except otherwise agreed by both
//    parties in writing.
//
// 5. If requested, MStar may from time to time provide technical supports or
//    services in relation with MStar Software to you for your use of
//    MStar Software in conjunction with your or your customer`s product
//    ("Services").
//    You understand and agree that, except otherwise agreed by both parties in
//    writing, Services are provided on an "AS IS" basis and the warranty
//    disclaimer set forth in Section 4 above shall apply.  
//
// 6. Nothing contained herein shall be construed as by implication, estoppels
//    or otherwise:
//    (a) conferring any license or right to use MStar name, trademark, service
//        mark, symbol or any other identification;
//    (b) obligating MStar or any of its affiliates to furnish any person,
//        including without limitation, you and your customers, any assistance
//        of any kind whatsoever, or any information; or 
//    (c) conferring any license or right under any intellectual property right.
//
// 7. These terms shall be governed by and construed in accordance with the laws
//    of Taiwan, R.O.C., excluding its conflict of law rules.
//    Any and all dispute arising out hereof or related hereto shall be finally
//    settled by arbitration referred to the Chinese Arbitration Association,
//    Taipei in accordance with the ROC Arbitration Law and the Arbitration
//    Rules of the Association by three (3) arbitrators appointed in accordance
//    with the said Rules.
//    The place of arbitration shall be in Taipei, Taiwan and the language shall
//    be English.  
//    The arbitration award shall be final and binding to both parties.
//
//******************************************************************************
//<MStar Software>
////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (��MStar Confidential Information��) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>

#include "MsCommon.h"
#include "MsIRQ.h"
#include "MsOS.h"
#include "hwreg.h"
#include "sysinfo.h"

#include "drvUartDebug.h"
#include "drvISR.h"
#include "drvCPU.h"
#include "SysInit.h"
#include "Analog_Reg.h"
#include "drvpower_if.h"
#include "GPIO.h"
#include "drvIIC.h"

#include "msIR.h"
#include "drvAUDIO.h"
#include "drvGlobal.h"
#include "msKeypad.h"
#include "apiXC_Adc.h"
#include "drvIR.h"

//grey out due to non-used
//extern BOOLEAN Drv_Power_CheckOnTimer(void);
static IR_RegCfg   _tIrRegCfg;

#define DBGNUM(x, y)            //{putchar(x);putchar(y);putchar('\n');}

//------------------------------------------------------------------------------
/// Execute power on initialization.
/// @param
/// @return TRUE- Success.
///         FALSE - Failure.
//------------------------------------------------------------------------------

#ifndef MSOS_TYPE_LINUX
void USB_Init_Configure(void)
{
   // printf("\r\n USB_Init_Configure");
    //MDrv_WriteRegBit(0x25C0, DISABLE, BIT4); //Enable current source in DVI
    MDrv_WriteByte(0x25C0, 0x0); //Enable current source in DVI

    //open port 0
    MDrv_WriteRegBit(0x3aac, ENABLE, BIT2); //Select current switch from DVI
    MDrv_WriteRegBit(0x3a86, ENABLE, BIT2); //HSTXIEN
    MDrv_WriteByte(0x3a80, 0x0);            //disable power down over write
    MDrv_WriteByte(0x3a81, 0x0);            //disable power down over write high byte
    MDrv_WriteRegBit(0x0700, ENABLE, 0x28); //disable init suspend state
    MsOS_DelayTask(3);
    MDrv_WriteRegBit(0x0702, ENABLE, BIT0);  //UHC select enable
    //end

    // Power On USB
    MDrv_WriteRegBit(0x2434, ENABLE, BIT6);
    MsOS_DelayTask(2);
    MDrv_WriteRegBit(0x2440, DISABLE, BIT4);

    //open port 1
    MDrv_WriteRegBit(0x3a2c, ENABLE, BIT2); //Select current switch from DVI
    MDrv_WriteRegBit(0x3a06, ENABLE, BIT2); //HSTXIEN
    MDrv_WriteByte(0x3a00, 0x0);   //disable power down over write
    MDrv_WriteByte(0x3a01, 0x0);   //disable power down over write high byte
    MDrv_WriteRegBit(0x0780, ENABLE, 0x28); //disable init suspend state
    MsOS_DelayTask(3);
    MDrv_WriteRegBit(0x0782, ENABLE, BIT0);  //UHC select enable
    //end

    // Power On USB
    MDrv_WriteRegBit(0x0d34, ENABLE, BIT6);
    MsOS_DelayTask(2);
    MDrv_WriteRegBit(0x0d40, DISABLE, BIT4);

    #if 0
    //open port 1
    MDrv_WriteRegBit(0x3aec, ENABLE, BIT2);         //Select current switch from DVI
    MDrv_WriteRegBit(0x3ac6, ENABLE, BIT7|BIT2);    //bit2 HSTXIEN, bit7 otg dull role mode
    MDrv_WriteByte(0x3ac0, 0x0);                    //disable power down over write
    MDrv_WriteByte(0x3ac1, 0x0);                    //disable power down over write high byte
    MDrv_WriteRegBit(0x0700, ENABLE, BIT4);         //disable init suspend state
    MsOS_DelayTask(3);
    MDrv_WriteRegBit(0x0702, ENABLE, BIT6|BIT5);    //bit5 Enable Port1 UTMI, bit6 port idpull up
    //end
    #endif

   // printf("\r\n USB_Init_Configure end=%x\n",MDrv_ReadByte(0x25C0));
}
#endif

BOOLEAN MDrv_Power_ExecutePowerUp()
{
    // Change power management status
    MDrv_WriteByteMask(PM_OFF_FLAG, PM_MODE_ON, PM_MODE_MASK);
    MDrv_WriteRegBit(PM_OFF_FLAG, DISABLE, PM_P3_RESET_FLAG);

#if ENABLE_POWER_SAVING_DPMS
    MDrv_WriteRegBit(PM_OFF_FLAG, DISABLE, VGA_POWERSAVING);
#endif

#ifndef MSOS_TYPE_LINUX
    DBGNUM('P', '0');
    USB_Init_Configure();

    DBGNUM('P', '1');
#endif
    // Power on PCMCIA
    MDrv_WriteByteMask(0x1E12, 0, 0xFC);
    MDrv_WriteByteMask(0x1E13, 0, 0xFF);
    MDrv_WriteByteMask(0x1E14, 0, 0xFF);
    MDrv_WriteByteMask(0x1E15, 0, 0xFF);
    MDrv_WriteByteMask(0x1E16, 0, 0xFF);
    MDrv_WriteByteMask(0x1E17, 0, 0x7F);

    MDrv_WriteByteMask(0x1E1A, 0, 0xFC);
    MDrv_WriteByteMask(0x1E1B, 0, 0xFF);
    MDrv_WriteByteMask(0x1E1C, 0, 0xFF);
    MDrv_WriteByteMask(0x1E1D, 0, 0xFF);
    MDrv_WriteByteMask(0x1E1E, 0, 0xFF);
    MDrv_WriteByteMask(0x1E1F, 0, 0x7F);

#ifdef TEST_POWER_DOWN
    MDrv_WriteByte(0x1C00,0x5F);
    MDrv_WriteByte(0x1C01,0xFF);
#endif

    DBGNUM('P', '2');

    return TRUE;
}

//-------------------------------------------------------------------------------------------------
/// Power down procedure: after reboot, the main loop will execute power down procedure, and enter standby mode then
/// @param
/// @return
///
//-------------------------------------------------------------------------------------------------
void MDrv_Power_ExecutePowerDown()
{
//    bPowerOnMode = 0;    // set power flag to standby mode

    MDrv_WriteByte(0x1C00, 0x00);
    MDrv_WriteByte(0x1C01, 0x00);

    // Change power management status
    MDrv_WriteByteMask(PM_OFF_FLAG, PM_MODE_OFF_EXEC, PM_MODE_MASK);
    MDrv_WriteRegBit(PM_OFF_FLAG, ENABLE, PM_P3_RESET_FLAG);

    // Disable interrupt.
    mhal_interrupt_disable();

    // Disable VE
    MDrv_WriteByte(0x3B00, 0);      // Set En_CCIR, En_TVE, En_VE to 0
    MDrv_WriteByte(0x3B01, 0);

    // Set scaler to frame buffer less
    MDrv_WriteByte( BK_SELECT_00, REG_BANK_DNR );
    MDrv_WriteRegBit( L_BK_DNR(0x60), 1, BIT0 );

    // Disable 3D comb
    MDrv_WriteByte(BK_COMB_10, 0x02);

    // Disable VBI Teletext and CC
    MDrv_WriteByte(0x3710, 0x00);
    MDrv_WriteByte(BK_VBI_46, 0x00);

    _tIrRegCfg.u8Clk_mhz = MST_XTAL_CLOCK_MHZ;
    MDrv_IR_Init(&_tIrRegCfg);

    // Switch SPI clock to XTAL
    MDrv_WriteRegBit(0x3C4C, DISABLE, BITMASK(5:2));
    MDrv_WriteRegBit(0x3C41, DISABLE, BIT7);         // Disable fast read

    MDrv_Sys_RunCodeInSPI();

    // Switch MCU clock to XTAL on SPI to prevent code fetch problem.
    MDrv_WriteByte(0x1E22, 0x00);
    MDrv_Sys_InitUartForXtalClk( '#' );

    // Setup MIU request mask
    MDrv_WriteByte(0x1246, 0x1E);
    MDrv_WriteByte(0x1247, 0xFF);
    MDrv_WriteByte(0x1266, 0xFF);
    MDrv_WriteByte(0x1267, 0xFF);
    MDrv_WriteByte(0x1286, 0xFF);
    MDrv_WriteByte(0x1287, 0xFF);

    // Reset engines
    MDrv_WriteByte(MVD_CTRL, 0x01);         // MVD
    MDrv_WriteByte(0x1422, 0x00);           // MVOP
    MDrv_WriteByte(0x1086, 0x00);           // AEON
    MDrv_WriteByte(TSP_CTRL, 0x00);         // TSP
    MDrv_WriteRegBit(VD_MCU_RESET, ENABLE, BIT0);    // VDMCU
    MDrv_WriteByte(BK_VBI_70, BIT3);        // VBI
    MDrv_WriteRegBit(BK_COMB_13, ENABLE, BIT6);      // COMB
    MDrv_WriteByte(0x2800, 0x00);           // PE
    MDrv_WriteByte(0x2802, 0x00);           // PE_CMQ
    MDrv_WriteByte(0x1FFF, 0x02);           // GOP force write
    MDrv_WriteByte(0x1FFE, 0x00);           // GOP0
    MDrv_WriteByte(0x1F00, 0x50);
    MDrv_WriteByte(0x1FFE, 0x01);           // GOP1
    MDrv_WriteByte(0x1F00, 0x50);
    MDrv_WriteByte(0x1FFE, 0x02);           // GOPD
    MDrv_WriteByte(0x1F00, 0x50);

    /// Power down audio
    MDrv_WriteByteMask(0x2CE6, 0x40, 0x40); // Audio ADC input Mixer power down
    MDrv_WriteByteMask(0x2CEA, 0x00, 0x03); // power down AA0 & AA1
    MDrv_WriteByteMask(0x2CEE, 0x00, 0x30); // Power Down Audio DAC[2]
    MDrv_WriteByteMask(0x2CEC, 0x00, 0x34); // Power Down Audio DAC[1]
    MDrv_WriteByteMask(0x2CED, 0x00, 0x33); // Power Down Audio DAC[0]/[1] Opam
    MDrv_WriteByteMask(0x2CC1, 0x18, 0x18); // turn off SIF ADC
    MDrv_WriteByteMask(0x2CC0, 0x0C, 0x0C); // turn off SIF ADC
    MDrv_WriteByteMask(0x2CC2, 0x03, 0x03); // turn off SIF ADC Vref Gen.
    MDrv_WriteByteMask(0x2CD2, 0x10, 0x10); // turn off SIF PLL
    MDrv_WriteByteMask(0x2CD3, 0x03, 0x03); // power down PLL divider
    MDrv_WriteByteMask(0x2C20, 0x08, 0x08); // turn off  HDMI PLL

    /// Power down AU
    MDrv_WriteByte(0x2C02, 0x00);  // Disable all Freq. synthesizer
    MDrv_WriteByte(0x2C03, 0x00);
    MDrv_WriteByte(0x2CAF, 0x00); // Disable all Synthizer clock.
    MDrv_WriteByte(0x2CAA, 0x00);
    MDrv_WriteByte(0x2CAB, 0x00);
    MDrv_WriteByte(0x2CE2, 0x00);  // power down Vref gen.
    MDrv_WriteByte(0x2CE5, 0xE0);  // Left/Right channel ADC power down

    /// power down SIF PLL
    MDrv_WriteRegBit(0x2CC2, ENABLE, 0x03);
    MDrv_WriteRegBit(0x2CD2, ENABLE, 0x10);

    /// Power down Scaler
    g_IPanel.Enable(DISABLE);

    MApi_XC_ADC_PowerOff();

    // should be changed to caller in API layer to stop timing monitor instead of using like this here.
    //if(IsSrcTypeAnalog(SYS_INPUT_SOURCE_TYPE) || IsSrcTypeHDMI(SYS_INPUT_SOURCE_TYPE) || IsSrcTypeDVI(SYS_INPUT_SOURCE_TYPE) )
    //{
        //g_bInputTimingChange = FALSE;     // Daten, move to apiXC_PCMonitor.c internal variable
    //}

    // power down all ADC
    MDrv_WriteByte(0x2508, 0xFF);
    MDrv_WriteByte(0x2509, 0xFF);

#if ENABLE_POWER_SAVING_DPMS
    if(MDrv_ReadByte(PM_OFF_FLAG) & VGA_POWERSAVING)
    {
        MDrv_WriteByte(0x2F00, REG_BANK_IP1F2);
        // FIXME .. DO not access register directly here.
        MDrv_WriteByte(0x2F04, (INPUT_VGA_MUX - INPUT_PORT_ANALOG0 & 0x7) | ( ((AUTO_DETECT << 1) | CSYNC)  << 4 ));
        MDrv_WriteByte(0x250A, 0x1F);
    }
    else
    {
        MDrv_WriteByte(0x250A, 0xFF);
    }
#else
    MDrv_WriteByte(0x250A, 0xFF);
#endif
    /// power down fast blanking ADC
    MDrv_WriteRegBit(0x2580, ENABLE, BIT6);

    /// power down DVI
    MDrv_WriteByte(0x25C0, 0xFF);
    MDrv_WriteByte(0x25C1, 0xFF);

    /// Power down OTG
    MDrv_WriteRegBit(0x3A80, DISABLE, BIT0);         // disable override power down mode
//    MDrv_WriteRegBit(0x2440, ENABLE, BIT4);
    MDrv_WriteRegBit(0x2434, DISABLE, BIT6);          // set suspend

    /// power down DAC
    MDrv_WriteRegBit(0x3209, ENABLE, 0x02);

    /// Disable DDR PAD
//    MDrv_WriteRegBit(0x1202, DISABLE, 0xC0);
//    MDrv_WriteRegBit(0x1203, ENABLE, 0xF0);

//    MDrv_WriteRegBit(0x1202, DISABLE, 0xB0);  //for DDRII_x16 interface

    MDrv_WriteRegBit(0x1202, DISABLE, 0xA0);  //for DDRII_x32 interface
    MDrv_WriteRegBit(0x1203, ENABLE, 0xF0);

    /// Power down DACA, DACR, DACG, DACB
    MDrv_WriteRegBit(0x3206, ENABLE, 0x0F);

    /// Power down VCOM
    MDrv_WriteRegBit(0x3205, ENABLE, 0x02);

    /// Disable HW clock
    MDrv_Power_Set_HwClock(E_HWCLK_GE_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_GOP0_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_GOP1_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_GOPD_ONOFF, POWER_DOWN);

    MDrv_Power_Set_HwClock(E_HWCLK_MVD_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_MVDBOOT_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_M4V_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_DC0_ONOFF, POWER_DOWN);

    MDrv_Power_Set_HwClock(E_HWCLK_MADSTC_ONOFF, POWER_DOWN);

    MDrv_Power_Set_HwClock(E_HWCLK_TS0_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_TS2_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_TSOUT_ONOFF, POWER_DOWN);

    MDrv_Power_Set_HwClock(E_HWCLK_FCLK_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_FMCLK_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_ODCLK_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_IDCLK2_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_FICLKF2_ONOFF, POWER_DOWN);

    MDrv_Power_Set_HwClock(E_HWCLK_VEIN_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_VE_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_VEDAC_ONOFF, POWER_DOWN);

    MDrv_Power_Set_HwClock(E_HWCLK_VD_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_VDMCU_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_VD200_ONOFF, POWER_DOWN);

    MDrv_Power_Set_HwClock(E_HWCLK_DHCSBM_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_DHCDDR_GATING, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_DHCSYNTH_GATING, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_DHCMCU_GATING, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_DHCLIVE_GATING, POWER_DOWN);

    MDrv_Power_Set_HwClock(E_HWCLK_USB_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_FCIE_ONOFF, POWER_DOWN);
    MDrv_Power_Set_HwClock(E_HWCLK_PCMCIA_ONOFF, POWER_DOWN);

    // Disable XDATA Mapping
    MDrv_WriteRegBit(0x2BC4, DISABLE, BIT2);

    /// Power down PCMCIA
    MDrv_WriteByte(0x1E10, 0xFF);
    MDrv_WriteByte(0x1E11, 0xFF);
    MDrv_WriteByte(0x1E12, 0xFF);
    MDrv_WriteByte(0x1E13, 0xFF);
    MDrv_WriteByte(0x1E14, 0xFF);
    MDrv_WriteByte(0x1E15, 0xFF);
    MDrv_WriteByte(0x1E16, 0xFF);
    MDrv_WriteByte(0x1E17, 0x03);
    MDrv_WriteByte(0x1E18, 0xFF);
    MDrv_WriteByte(0x1E19, 0xFF);
    MDrv_WriteByte(0x1E1A, 0xFF);
    MDrv_WriteByte(0x1E1B, 0xFF);
    MDrv_WriteByte(0x1E1C, 0xFF);
    MDrv_WriteByte(0x1E1D, 0xFF);
    MDrv_WriteByte(0x1E1E, 0xFF);
    MDrv_WriteByte(0x1E1F, 0x03);

    /// power down LPLL
    MDrv_WriteRegBit(0x3106, ENABLE, 0x20);

    /// Power down DDRPLL
    MDrv_WriteRegBit(0x1225, ENABLE, 0x02);

    MDrv_WriteRegBit(0x1e25, ENABLE, BIT4 | BIT0);

    // Power down MPLL
    MDrv_WriteByte(0x2510, 0x10);

    // Change power management status
    MDrv_WriteByteMask(PM_OFF_FLAG, PM_MODE_OFF, PM_MODE_MASK);
    MDrv_WriteRegBit(PM_OFF_FLAG, DISABLE, PM_P3_RESET_FLAG);

    #if ( POWER_CONTROL == ENABLE ) //mask temp for eris bright up
        MDrv_WriteByte(0x1EE4, 0);     // Disable VDD2LOW_CTRL
        POWER_ON_OFF1_Off();
    #endif

    MDrv_WriteByte(0x1202, MDrv_ReadByte(0x1202) & 0x3F);

}
/*
//-------------------------------------------------------------------------------------------------
/// Standby mode Check OnTimer Day of Week
/// @param
/// @return
//-------------------------------------------------------------------------------------------------
BOOLEAN Drv_Power_CheckOnTimer(void)
{
    switch(g_eOnTimerDateForStandBy)
    {
        case EN_OnTimer_Off:
            gWakeupSystemTime = 0xffffffff;
            return FALSE;

        case EN_OnTimer_Once:
            g_eOnTimerDate = EN_OnTimer_Off;
            gWakeupSystemTime = 0xffffffff;
            return TRUE;

        case EN_OnTimer_Everyday:
            return TRUE;

        case EN_OnTimer_Mon2Fri:
            if(g_eDayOfWeekForStandBy==SAT || g_eDayOfWeekForStandBy==SUN )    //SAT or SUN
            {
                return FALSE;
            }
            else     //MON~FRI
            {
                return TRUE;
            }

        case EN_OnTimer_Mon2Sat:
            if(g_eDayOfWeekForStandBy==SUN)    //sun
            {
                return FALSE;
            }
            else
            {
                return TRUE;
            }

        case EN_OnTimer_Sat2Sun:
            if(g_eDayOfWeekForStandBy==SUN || g_eDayOfWeekForStandBy == SAT )
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }

        case EN_OnTimer_Sun :
            if(g_eDayOfWeekForStandBy==SUN)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }

        default:
            return FALSE;
    }
}
*/

// not used.
#if 0//ENABLE_POWER_SAVING_DPMS
////////////////////////////////////////////////////////////////////////////////
/// @brief \b Function \b Name: MDrv_Power_VGAWakeUp
/// @brief \b Function \b Description: Wake up VGA port
/// @param <IN>        \b None:
/// @param <OUT>     \b None :
/// @param <RET>      \b None :
/// @param <GLOBAL> \b None :
////////////////////////////////////////////////////////////////////////////////
void MDrv_Power_VGAWakeUp()
{
    U16 data u16Value;
    // check H-Period and V-Total
    MDrv_WriteByte(BK_SELECT_00, REG_BANK_IP1F2);
    u16Value = MDrv_Read2Byte( L_BK_IP1F2(0x20)) & 0x1FFF ;
    if(u16Value > 10 && u16Value != 0x1FFF)
    {
        u16Value = MDrv_Read2Byte( L_BK_IP1F2(0x1F)) & 0x07FF;
        if(u16Value > 200 && u16Value != 0x7FF)
        {
            MDrv_Power_ResetAndPowerUp();
        }
    }
}
#endif

//-------------------------------------------------------------------------------------------------
/// Power standby: After execution MDrvPowerExecutePowerDown, the MDrvPowerStandby will be called
/// @param  u32StandbyS \b IN: Specify the standby time in seconds.
///                            If u32StandbyS is equal to zero, will enter standby mode forever.
/// @return
///
//-------------------------------------------------------------------------------------------------
/*
U8 KeypadCount, KeypadSamplePeriod;
void MDrv_Power_Standby()
{
    U32 u32PrevSystemTimeCount;

    KeypadCount = 0;
    KeypadSamplePeriod = 0;

#if (IR_MODE_SEL != IR_TYPE_FULLDECODE_MODE)
    mhal_interrupt_unmask( MHAL_INTERRUPT_FIQ );
    mdrv_irq_unmask( FIQ_IR );
#endif

#if( POWER_KEY_PAD_BY_INTERRUPT )
    MDrv_Sys_SetInterrupt( EX0, ENABLE );
    MDrv_Sys_SetInterrupt( EX0_INT_IN, ENABLE );
    msKeypad_Set_PwrKey_IntFlag(FALSE);
#endif

    u32PrevSystemTimeCount = gSystemTimeCount;        // on timer
    //StandbyEepromAccessTest();

    #if 0
    #if (IR_MODE_SEL == IR_TYPE_SWDECODE_SHA_MODE)
    gStandbyInto=TRUE;
    #endif
    #endif

    while(1)
    {
        #if ( WATCH_DOG == ENABLE )
        MDrv_Sys_ClearWatchDog();
        #endif

#if (IR_MODE_SEL != IR_TYPE_SWDECODE_SHA_MODE)
        // IR Wakeup
        if (MDrv_Power_CheckPowerOnKey())
            MDrv_Power_ResetAndPowerUp();
#endif

#if ((IR_MODE_SEL != IR_TYPE_FULLDECODE_MODE)&&(IR_MODE_SEL != IR_TYPE_SWDECODE_SHA_MODE))
        if (MDrv_Power_CheckPowerOnSWIRKey())
            MDrv_Power_ResetAndPowerUp();
#endif
#if (IR_MODE_SEL == IR_TYPE_SWDECODE_SHA_MODE)
    if (MDrv_CheckSWIRKey())
        MDrv_Power_ResetAndPowerUp();
#endif
#if (KEYPAD_TYPE_SEL != KEYPAD_TYPE_NONE)
        // Keypad Wakeup
        if (MDrv_Power_CheckPowerOnKeyPad())
            MDrv_Power_ResetAndPowerUp();
#endif

#if ENABLE_POWER_SAVING_DPMS
        if(XBYTE[PM_OFF_FLAG] & VGA_POWERSAVING)
        {
            MDrv_Power_VGAWakeUp();
        }
#endif

        // Current use Timer2 for RTC function
        #if 1
//        if (gSystemTimeCount >= gWakeupSystemTime)
//            MDrv_Power_ResetAndPowerUp();

        // on timer
        if((gSystemTimeCount % 86400 == 0) && (u32PrevSystemTimeCount != gSystemTimeCount))
        {
            g_eDayOfWeekForStandBy++;
            g_eDayOfWeekForStandBy=(DAYOFWEEK) (g_eDayOfWeekForStandBy%7);        //sun 0 mon 1 tue 2 wed 3 thu 4 fri 5 sat 6

            u32PrevSystemTimeCount = gSystemTimeCount;
        }

        if ( gWakeupSystemTime != 0xffffffff && ((gSystemTimeCount % 86400) == (gWakeupSystemTime % 86400)))
        {
            if( Drv_Power_CheckOnTimer() == TRUE )
            {
                g_bWakeUpByOnTimer = TRUE;
                MDrv_Power_ResetAndPowerUp();
            }
        }
        #else
        if (gRTCWakeup == TRUE)
        {
            MDrv_Power_ResetAndPowerUp();
            gRTCWakeup = FALSE;
        }
        #endif
    }
}
*/
