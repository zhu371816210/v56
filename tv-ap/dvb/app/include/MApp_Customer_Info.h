////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2009 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (��MStar Confidential Information��) by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////


/* INPUT_SW_PROJECT
Chakra      -   01
POLLUX      -   02
ARCHIMEDES  -   03
Chakra2     �V   04
OBAMA       �V   05
*/

/* INPUT_PRODUCT_TYPE
TV-01
STB-02
*/

/* INPUT_TV_SYSTEM
ATSC-01
DVBT-02
DVBC-03
DVBS-04
DMBT-05
ATV Only-06
*/
#define INPUT_CUSTOMER_ID_LOW_BYTE  0x04
#define INPUT_CUSTOMER_ID_HIGH_BYTE 0x60

//Model ID (Example:0001 = 0x0001)
#define INPUT_MODEL_ID_LOW_BYTE     0x01
#define INPUT_MODEL_ID_HIGH_BYTE    0x00

//Chip ID (Example:000B = 0x000B)
#define INPUT_CHIP_ID_LOW_BYTE      0x66
#define INPUT_CHIP_ID_HIGH_BYTE     0x66

#define INPUT_SW_PROJECT            0x04
#define INPUT_SW_GENERATION         0x01
#define INPUT_PRODUCT_TYPE          0x01
#define INPUT_TV_SYSTEM             0x02

//Label (Example:00000456 = 0x0001C8)
#define INPUT_LABEL_LOW_BYTE        0x08
#define INPUT_LABEL_MIDDLE_BYTE     0x00
#define INPUT_LABEL_HIGH_BYTE       0x00

//CL(Change-List) (Example:00101234 = 0x18B72)
#define INPUT_CL_LOW_BYTE           0x10
#define INPUT_CL_MIDDLE_BYTE        0x4C
#define INPUT_CL_HIGH_BYTE          0x02

#define DRM_MODEL_ID  0x3130


//  SW Quality
//  01-BOOTLEG
//  02-Demo
//  03-Pre-Alpha
//  04-Alpha
//  05-Beta
//  06-RC
//  07-RTM
//  Quality flag can be modified by release engineer only

#define INPUT_RELEASE_QUALITY       0x01

//CPU TYPE
//01-MIPS
//02-AEON
#if (defined(MIPS_CHAKRA) || defined(ARM_CHAKRA))
#define  INPUT_CPU_TYPE 0x01
#else
#define  INPUT_CPU_TYPE 0x02
#endif

//Customer IP
//DD, MPEG2, MPEG2_HD, MPEG4, MPEG4_SD, MPEG4_SD, H.264,DivX_DRM, WMA, RM, VC1 DivX Home Theater, FLV  583c5575eb360cf6d4105ec41e2b15ee
unsigned char code IP_Cntrol_Mapping_1[8] ="00000000"; //Customer IP Control-1
unsigned char code IP_Cntrol_Mapping_2[8] ="00000000"; //Customer IP Control-2
unsigned char code IP_Cntrol_Mapping_3[8] ="000007FF"; //Customer IP Control-3
unsigned char code IP_Cntrol_Mapping_4[8] ="FFFFFFFF"; //Customer IP Control-4

unsigned char code Customer_hash[] = {0xa9,0x15,0xdc,0x3e,0x93,0x4a,0xf9,0xae,0x06,0x1b,0x38,0x92,0x34,0xc4,0x6f,0xc0};
//{0x58,0x3c,0x55,0x75,0xeb,0x36,0x0c,0xf6,0xd4,0x10,0x5e,0xc4,0x1e,0x2b,0x15,0xee};

U16 u16DRMModelID=DRM_MODEL_ID;
//**************************************************************************
//**************************************************************************
//**************************************************************************





unsigned char code CID_Buf[32] =  {
//Fix Value: Do not Modify
        'M', 'S', 'I', 'F',         // (Do not modify)Mstar Information:MSIF
        '0', '1',                   // (Do not modifyCustomer Info Class: 01

//Customer Info area
        INPUT_CUSTOMER_ID_LOW_BYTE,
        INPUT_CUSTOMER_ID_HIGH_BYTE,

        INPUT_MODEL_ID_LOW_BYTE,
        INPUT_MODEL_ID_HIGH_BYTE,

        INPUT_CHIP_ID_LOW_BYTE,
        INPUT_CHIP_ID_HIGH_BYTE,

        INPUT_SW_PROJECT,
        INPUT_SW_GENERATION,
        INPUT_PRODUCT_TYPE,
        INPUT_TV_SYSTEM,

        INPUT_LABEL_LOW_BYTE,
        INPUT_LABEL_MIDDLE_BYTE,
        INPUT_LABEL_HIGH_BYTE,

        INPUT_CL_LOW_BYTE,
        INPUT_CL_MIDDLE_BYTE,
        INPUT_CL_HIGH_BYTE,

        INPUT_RELEASE_QUALITY,

        INPUT_CPU_TYPE,
//Reserve
        '0', '0', '0', '0', '0', '0', '0', '0'   // Reserve
        };

