////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2010 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (''MStar Confidential Information'') by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// This file is automatically generated by SkinTool [Version:0.2.3][Build:Dec 18 2014 17:25:00]
/////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////
// Window List (Position)

WINDOWPOSDATA _MP_TBLSEG _GUI_WindowPositionList_Zui_Dtv_Manual_Tuning[] =
{
    // HWND_MAINFRAME
    {
        HWND_MAINFRAME, HWND_DTUNE_SCAN_ENTERKEY_REMIND_TXT,
        { 0, 0, 640, 360 },
    },

    // HWND_DTUNE_BG_PANE
    {
        HWND_MAINFRAME, HWND_DTUNE_BG_PANE_DEC,
        { 365, 21, 250, 192 },
    },

    // HWND_DTUNE_BG_PANE_L
    {
        HWND_DTUNE_BG_PANE, HWND_DTUNE_BG_PANE_L,
        { 365, 21, 1, 192 },
    },

    // HWND_DTUNE_BG_PANE_C
    {
        HWND_DTUNE_BG_PANE, HWND_DTUNE_BG_PANE_C,
        { 368, 21, 246, 192 },
    },

    // HWND_DTUNE_BG_PANE_R
    {
        HWND_DTUNE_BG_PANE, HWND_DTUNE_BG_PANE_R,
        { 614, 21, 1, 192 },
    },

    // HWND_DTUNE_TITLE_TXT
    {
        HWND_DTUNE_BG_PANE, HWND_DTUNE_TITLE_TXT,
        { 412, 21, 156, 30 },
    },

    // HWND_DTUNE_BG_PANE_LEFT_ARROW
    {
        HWND_DTUNE_BG_PANE, HWND_DTUNE_BG_PANE_LEFT_ARROW,
        { 385, 63, 7, 7 },
    },

    // HWND_DTUNE_BG_PANE_RIGHT_ARROW
    {
        HWND_DTUNE_BG_PANE, HWND_DTUNE_BG_PANE_RIGHT_ARROW,
        { 588, 63, 7, 7 },
    },

    // HWND_DTUNE_BG_PANE_INC
    {
        HWND_DTUNE_BG_PANE, HWND_DTUNE_BG_PANE_INC,
        { 574, 63, 10, 10 },
    },

    // HWND_DTUNE_BG_PANE_DEC
    {
        HWND_DTUNE_BG_PANE, HWND_DTUNE_BG_PANE_DEC,
        { 395, 63, 12, 10 },
    },

    // HWND_DTUNE_MANUAL_SCAN_BAR
    {
        HWND_MAINFRAME, HWND_DTUNE_MANUAL_SCAN_CHANNEL_NAME_TXT,
        { 365, 51, 250, 30 },
    },

    // HWND_DTUNE_MANUAL_SCAN_UFH_TXT
    {
        HWND_DTUNE_MANUAL_SCAN_BAR, HWND_DTUNE_MANUAL_SCAN_UFH_TXT,
        { 421, 51, 73, 30 },
    },

    // HWND_DTUNE_MANUAL_SCAN_CHANNEL_NAME_TXT
    {
        HWND_DTUNE_MANUAL_SCAN_BAR, HWND_DTUNE_MANUAL_SCAN_CHANNEL_NAME_TXT,
        { 503, 51, 73, 30 },
    },

    // HWND_DTUNE_SCAN_RESULT
    {
        HWND_MAINFRAME, HWND_DTUNE_SCAN_RESULT_DATA_NUMBER_TXT,
        { 365, 84, 250, 100 },
    },

    // HWND_DTUNE_SCAN_RESULT_TXT
    {
        HWND_DTUNE_SCAN_RESULT, HWND_DTUNE_SCAN_RESULT_DATA_NUMBER_TXT,
        { 365, 84, 205, 69 },
    },

    // HWND_DTUNE_SCAN_RESULT_DTV_TXT
    {
        HWND_DTUNE_SCAN_RESULT_TXT, HWND_DTUNE_SCAN_RESULT_DTV_NUMBER_TXT,
        { 417, 85, 69, 21 },
    },

    // HWND_DTUNE_SCAN_RESULT_DTV_NUMBER_TXT
    {
        HWND_DTUNE_SCAN_RESULT_DTV_TXT, HWND_DTUNE_SCAN_RESULT_DTV_NUMBER_TXT,
        { 501, 85, 69, 21 },
    },

    // HWND_DTUNE_SCAN_RESULT_RADIO_TXT
    {
        HWND_DTUNE_SCAN_RESULT_TXT, HWND_DTUNE_SCAN_RESULT_RADIO_NUMBER_TXT,
        { 416, 109, 69, 21 },
    },

    // HWND_DTUNE_SCAN_RESULT_RADIO_NUMBER_TXT
    {
        HWND_DTUNE_SCAN_RESULT_RADIO_TXT, HWND_DTUNE_SCAN_RESULT_RADIO_NUMBER_TXT,
        { 501, 109, 69, 21 },
    },

    // HWND_DTUNE_SCAN_RESULT_DATA_TXT
    {
        HWND_DTUNE_SCAN_RESULT_TXT, HWND_DTUNE_SCAN_RESULT_DATA_NUMBER_TXT,
        { 416, 132, 69, 21 },
    },

    // HWND_DTUNE_SCAN_RESULT_DATA_NUMBER_TXT
    {
        HWND_DTUNE_SCAN_RESULT_DATA_TXT, HWND_DTUNE_SCAN_RESULT_DATA_NUMBER_TXT,
        { 501, 132, 69, 21 },
    },

    // HWND_DTUNE_SCAN_SIGNAL_BG
    {
        HWND_MAINFRAME, HWND_DTUNE_SCAN_ENTERKEY_REMIND_TXT,
        { 378, 159, 205, 51 },
    },

    // HWND_DTUNE_SCAN_RESULT_NO_SIGNAL_BTN
    {
        HWND_DTUNE_SCAN_SIGNAL_BG, HWND_DTUNE_SCAN_RESULT_NO_SIGNAL_TXT,
        { 412, 162, 156, 13 },
    },

    // HWND_DTUNE_SCAN_RESULT_NO_SIGNAL_TXT
    {
        HWND_DTUNE_SCAN_RESULT_NO_SIGNAL_BTN, HWND_DTUNE_SCAN_RESULT_NO_SIGNAL_TXT,
        { 444, 160, 92, 18 },
    },

    // HWND_DTUNE_SCAN_RESULT_SEARCHING_TXT
    {
        HWND_DTUNE_SCAN_SIGNAL_BG, HWND_DTUNE_SCAN_RESULT_SEARCHING_TXT,
        { 404, 160, 172, 18 },
    },

    // HWND_DTUNE_BAD_TXT
    {
        HWND_DTUNE_SCAN_SIGNAL_BG, HWND_DTUNE_BAD_TXT,
        { 365, 180, 55, 18 },
    },

    // HWND_DTUNE_NORMAL_TXT
    {
        HWND_DTUNE_SCAN_SIGNAL_BG, HWND_DTUNE_NORMAL_TXT,
        { 463, 180, 55, 18 },
    },

    // HWND_DTUNE_GOOD_TXT
    {
        HWND_DTUNE_SCAN_SIGNAL_BG, HWND_DTUNE_GOOD_TXT,
        { 556, 180, 55, 18 },
    },

    // HWND_DTUNE_SIGNAL_BG
    {
        HWND_DTUNE_SCAN_SIGNAL_BG, HWND_DTUNE_SIGNAL_BAR,
        { 379, 198, 222, 6 },
    },

    // HWND_DTUNE_SIGNAL_BAR
    {
        HWND_DTUNE_SIGNAL_BG, HWND_DTUNE_SIGNAL_BAR,
        { 379, 198, 222, 6 },
    },

    // HWND_DTUNE_SCAN_ENTERKEY_REMIND_TXT
    {
        HWND_DTUNE_SCAN_SIGNAL_BG, HWND_DTUNE_SCAN_ENTERKEY_REMIND_TXT,
        { 406, 160, 172, 18 },
    },

};
