////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2010 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (''MStar Confidential Information'') by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// This file is automatically generated by SkinTool [Version:0.2.3][Build:Dec 18 2014 17:25:00]
/////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////
// Window List (Position)

WINDOWPOSDATA _MP_TBLSEG _GUI_WindowPositionList_Zui_Channel_Info[] =
{
    // HWND_MAINFRAME
    {
        HWND_MAINFRAME, HWND_CHINFO_POP_SECOND_SRC_ICON,
        { 0, 0, 640, 360 },
    },

    // HWND_CHINFO_TRANSPARENT_BG
    {
        HWND_MAINFRAME, HWND_CHINFO_TRANSPARENT_BG,
        { 0, 240, 640, 120 },
    },

    // HWND_CHINFO_ROOT_PANE
    {
        HWND_MAINFRAME, HWND_CHINFO_POP_SECOND_SRC_ICON,
        { 0, 243, 636, 115 },
    },

    // HWND_CHINFO_EXT_PANE
    {
        HWND_CHINFO_ROOT_PANE, HWND_CHINFO_EXT_S2_SIGNAL_STRENGTH_TEXT,
        { 0, 249, 636, 109 },
    },

    // HWND_CHINFO_EXT_TOP_BG
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_TOP_BG,
        { 634, 262, 2, 19 },
    },

    // HWND_CHINFO_EXT_TOP_BG_L
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_TOP_BG_L,
        { 0, 262, 632, 19 },
    },

    // HWND_CHINFO_EXT_CONTENT_BG_GREEN
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CONTENT_BG_GREEN,
        { 0, 283, 636, 18 },
    },

    // HWND_CHINFO_EXT_CONTENT_SIMPLE_BG
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CONTENT_SIMPLE_BG,
        { 0, 300, 636, 30 },
    },

    // HWND_CHINFO_EXT_CONTENT_EXTENSION_BG
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_UPDOWN_ARROW_ICON,
        { 0, 330, 636, 28 },
    },

    // HWND_CHINFO_EXT_CONTENT_TEXT
    {
        HWND_CHINFO_EXT_CONTENT_EXTENSION_BG, HWND_CHINFO_EXT_CONTENT_TEXT,
        { 21, 330, 594, 28 },
    },

    // HWND_CHINFO_EXT_UPDOWN_ARROW_ICON
    {
        HWND_CHINFO_EXT_CONTENT_EXTENSION_BG, HWND_CHINFO_EXT_UPDOWN_ARROW_ICON,
        { 618, 330, 17, 27 },
    },

    // HWND_CHINFO_EXT_RARROW_ICON
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_RARROW_ICON,
        { 618, 283, 17, 15 },
    },

    // HWND_CHINFO_EXT_LARROW_ICON
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_LARROW_ICON,
        { 0, 283, 17, 15 },
    },

    // HWND_CHINFO_EXT_CH_NUMBER_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CH_NUMBER_TEXT,
        { 22, 261, 45, 19 },
    },

    // HWND_CHINFO_EXT_CH_NAME_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CH_NAME_TEXT,
        { 79, 261, 85, 21 },
    },

    // HWND_CHINFO_EXT_TOP_TIME_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_TOP_TIME_TEXT,
        { 263, 261, 42, 19 },
    },

    // HWND_CHINFO_EXT_TOP_DATE_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_TOP_DATE_TEXT,
        { 167, 261, 88, 19 },
    },

    // HWND_CHINFO_EXT_TIME_START_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_TIME_START_TEXT,
        { 21, 282, 69, 15 },
    },

    // HWND_CHINFO_EXT_TIME_END_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_TIME_END_TEXT,
        { 92, 282, 69, 15 },
    },

    // HWND_CHINFO_EXT_NAME_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_NAME_TEXT,
        { 172, 282, 291, 18 },
    },

    // HWND_CHINFO_EXT_GENRE_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_GENRE_TEXT,
        { 474, 282, 102, 18 },
    },

    // HWND_CHINFO_EXT_AGE_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_AGE_TEXT,
        { 577, 282, 41, 18 },
    },

    // HWND_CHINFO_EXT_VIDEO_TYPE_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_VIDEO_TYPE_TEXT,
        { 17, 301, 94, 13 },
    },

    // HWND_CHINFO_EXT_VIDEO_RES_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_VIDEO_RES_TEXT,
        { 54, 301, 94, 13 },
    },

    // HWND_CHINFO_EXT_CH_COLOR_SYS_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CH_COLOR_SYS_TEXT,
        { 121, 301, 94, 15 },
    },

    // HWND_CHINFO_EXT_CH_SOUND_SYS_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CH_SOUND_SYS_TEXT,
        { 221, 301, 94, 15 },
    },

    // HWND_CHINFO_EXT_CH_INPUT_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CH_INPUT_TEXT,
        { 321, 301, 94, 15 },
    },

    // HWND_CHINFO_EXT_CH_SDHD_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CH_SDHD_TEXT,
        { 421, 301, 94, 15 },
    },

    // HWND_CHINFO_EXT_CH_MTS_TYPE_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CH_MTS_TYPE_TEXT,
        { 516, 301, 103, 15 },
    },

    // HWND_CHINFO_EXT_CH_AUDIO_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_CH_AUDIO_TEXT,
        { 21, 315, 94, 15 },
    },

    // HWND_CHINFO_EXT_MHEG_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_MHEG_TEXT,
        { 121, 315, 94, 15 },
    },

    // HWND_CHINFO_EXT_NARRATION_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_NARRATION_TEXT,
        { 221, 315, 94, 15 },
    },

    // HWND_CHINFO_EXT_SUBTITLE_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_SUBTITLE_TEXT,
        { 321, 315, 94, 15 },
    },

    // HWND_CHINFO_EXT_SUBTITLE_ICON
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_SUBTITLE_ICON,
        { 297, 315, 22, 15 },
    },

    // HWND_CHINFO_EXT_AUDIO_LANG_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_AUDIO_LANG_TEXT,
        { 421, 315, 94, 15 },
    },

    // HWND_CHINFO_EXT_TELETEXT_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_TELETEXT_TEXT,
        { 516, 315, 94, 15 },
    },

    // HWND_CHINFO_EXT_FIRST_SRC_ICON
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_FIRST_SRC_ICON,
        { 4, 267, 13, 9 },
    },

    // HWND_CHINFO_EXT_S2_SATLITENAME_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_S2_SATLITENAME_TEXT,
        { 303, 261, 140, 19 },
    },

    // HWND_CHINFO_EXT_S2_PREQ_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_S2_PREQ_TEXT,
        { 445, 261, 139, 19 },
    },

    // HWND_CHINFO_EXT_S2_SYMBOL_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_S2_SYMBOL_TEXT,
        { 265, 285, 94, 13 },
    },

    // HWND_CHINFO_EXT_S2_SIGNAL_STRENGTH_TEXT
    {
        HWND_CHINFO_EXT_PANE, HWND_CHINFO_EXT_S2_SIGNAL_STRENGTH_TEXT,
        { 421, 285, 94, 13 },
    },

    // HWND_CHINFO_PIP_SRC_INFO_AREA
    {
        HWND_CHINFO_ROOT_PANE, HWND_CHINFO_PIP_SRC2_VIDEO_RES_TEXT,
        { 0, 319, 640, 39 },
    },

    // HWND_CHINFO_PIP_SRC1_INFO_AREA
    {
        HWND_CHINFO_PIP_SRC_INFO_AREA, HWND_CHINFO_PIP_SRC1_VIDEO_RES_TEXT,
        { 0, 319, 640, 39 },
    },

    // HWND_CHINFO_PIP_SRC1_INFO_BG
    {
        HWND_CHINFO_PIP_SRC1_INFO_AREA, HWND_CHINFO_PIP_SRC1_INFO_BG,
        { 0, 319, 640, 39 },
    },

    // HWND_CHINFO_PIP_SRC1_ICON
    {
        HWND_CHINFO_PIP_SRC1_INFO_AREA, HWND_CHINFO_PIP_SRC1_ICON,
        { 3, 346, 9, 9 },
    },

    // HWND_CHINFO_PIP_SRC1_SOURCE_TYPE_TEXT
    {
        HWND_CHINFO_PIP_SRC1_INFO_AREA, HWND_CHINFO_PIP_SRC1_SOURCE_TYPE_TEXT,
        { 29, 319, 149, 39 },
    },

    // HWND_CHINFO_PIP_SRC1_VIDEO_RES_TEXT
    {
        HWND_CHINFO_PIP_SRC1_INFO_AREA, HWND_CHINFO_PIP_SRC1_VIDEO_RES_TEXT,
        { 179, 319, 199, 39 },
    },

    // HWND_CHINFO_PIP_SRC2_INFO_AREA
    {
        HWND_CHINFO_PIP_SRC_INFO_AREA, HWND_CHINFO_PIP_SRC2_VIDEO_RES_TEXT,
        { 0, 345, 636, 15 },
    },

    // HWND_CHINFO_PIP_SRC2_INFO_BG
    {
        HWND_CHINFO_PIP_SRC2_INFO_AREA, HWND_CHINFO_PIP_SRC2_INFO_BG,
        { 0, 345, 636, 15 },
    },

    // HWND_CHINFO_PIP_SRC2_ICON
    {
        HWND_CHINFO_PIP_SRC2_INFO_AREA, HWND_CHINFO_PIP_SRC2_ICON,
        { 2, 346, 13, 9 },
    },

    // HWND_CHINFO_PIP_SRC2_SOURCE_TYPE_TEXT
    {
        HWND_CHINFO_PIP_SRC2_INFO_AREA, HWND_CHINFO_PIP_SRC2_SOURCE_TYPE_TEXT,
        { 21, 345, 87, 13 },
    },

    // HWND_CHINFO_PIP_SRC2_VIDEO_RES_TEXT
    {
        HWND_CHINFO_PIP_SRC2_INFO_AREA, HWND_CHINFO_PIP_SRC2_VIDEO_RES_TEXT,
        { 114, 345, 87, 13 },
    },

    // HWND_CHINFO_POP_PANE
    {
        HWND_CHINFO_ROOT_PANE, HWND_CHINFO_POP_SECOND_SRC_ICON,
        { 0, 291, 636, 66 },
    },

    // HWND_CHINFO_POP_LEFT_AREA
    {
        HWND_CHINFO_POP_PANE, HWND_CHINFO_POP_FIRST_SRC_ICON,
        { 0, 255, 320, 102 },
    },

    // HWND_CHINFO_POP_LEFT_TV_INFO
    {
        HWND_CHINFO_POP_LEFT_AREA, HWND_CHINFO_POP_LEFT_CH_AUDIO_TEXT,
        { 0, 255, 317, 102 },
    },

    // HWND_CHINFO_POP_LEFT_TV_BG
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_TV_BG,
        { 0, 255, 317, 19 },
    },

    // HWND_CHINFO_POP_LEFT_TV_BG_DOWN
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_TV_BG_DOWN,
        { 0, 276, 317, 82 },
    },

    // HWND_CHINFO_POP_LEFT_TV_GREEN_BG
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_TV_GREEN_BG,
        { 0, 276, 317, 36 },
    },

    // HWND_CHINFO_POP_LEFT_CH_NUMBER_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_CH_NUMBER_TEXT,
        { 21, 255, 87, 19 },
    },

    // HWND_CHINFO_POP_LEFT_CH_NAME_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_CH_NAME_TEXT,
        { 114, 255, 87, 19 },
    },

    // HWND_CHINFO_POP_LEFT_TIME_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_TIME_TEXT,
        { 203, 255, 61, 19 },
    },

    // HWND_CHINFO_POP_LEFT_LARROW_ICON
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_LARROW_ICON,
        { 0, 283, 17, 15 },
    },

    // HWND_CHINFO_POP_LEFT_RARROW_ICON
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_RARROW_ICON,
        { 300, 283, 17, 15 },
    },

    // HWND_CHINFO_POP_LEFT_TIME_START_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_TIME_START_TEXT,
        { 21, 294, 43, 18 },
    },

    // HWND_CHINFO_POP_LEFT_TIME_END_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_TIME_END_TEXT,
        { 65, 294, 43, 18 },
    },

    // HWND_CHINFO_POP_LEFT_NAME_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_NAME_TEXT,
        { 21, 276, 102, 18 },
    },

    // HWND_CHINFO_POP_LEFT_GENRE_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_GENRE_TEXT,
        { 143, 294, 48, 18 },
    },

    // HWND_CHINFO_POP_LEFT_AGE_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_AGE_TEXT,
        { 187, 294, 28, 18 },
    },

    // HWND_CHINFO_POP_LEFT_TV_VIDEO_RES_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_TV_VIDEO_RES_TEXT,
        { 143, 315, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_CH_COLOR_SYS_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_CH_COLOR_SYS_TEXT,
        { 21, 330, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_CH_SOUND_SYS_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_CH_SOUND_SYS_TEXT,
        { 91, 330, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_CH_INPUT_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_CH_INPUT_TEXT,
        { 161, 330, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_CH_SDHD_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_CH_SDHD_TEXT,
        { 231, 330, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_CH_MTS_TYPE_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_CH_MTS_TYPE_TEXT,
        { 21, 315, 116, 15 },
    },

    // HWND_CHINFO_POP_LEFT_TELETEXT_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_TELETEXT_TEXT,
        { 231, 315, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_AUDIO_LANG_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_AUDIO_LANG_TEXT,
        { 231, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_SUBTITLE_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_SUBTITLE_TEXT,
        { 161, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_NARRATION_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_NARRATION_TEXT,
        { 161, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_MHEG_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_MHEG_TEXT,
        { 91, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_CH_AUDIO_TEXT
    {
        HWND_CHINFO_POP_LEFT_TV_INFO, HWND_CHINFO_POP_LEFT_CH_AUDIO_TEXT,
        { 21, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_LEFT_SRC_INFO
    {
        HWND_CHINFO_POP_LEFT_AREA, HWND_CHINFO_POP_LEFT_VIDEO_RES_TEXT,
        { 0, 288, 317, 69 },
    },

    // HWND_CHINFO_POP_LEFT_SRC_BG
    {
        HWND_CHINFO_POP_LEFT_SRC_INFO, HWND_CHINFO_POP_LEFT_SRC_BG,
        { 0, 255, 317, 19 },
    },

    // HWND_CHINFO_POP_LEFT_SRC_BG_LINE
    {
        HWND_CHINFO_POP_LEFT_SRC_INFO, HWND_CHINFO_POP_LEFT_SRC_BG_LINE,
        { 0, 276, 317, 82 },
    },

    // HWND_CHINFO_POP_LEFT_SOURCE_TYPE_TEXT
    {
        HWND_CHINFO_POP_LEFT_SRC_INFO, HWND_CHINFO_POP_LEFT_SOURCE_TYPE_TEXT,
        { 22, 315, 40, 13 },
    },

    // HWND_CHINFO_POP_LEFT_VIDEO_RES_TEXT
    {
        HWND_CHINFO_POP_LEFT_SRC_INFO, HWND_CHINFO_POP_LEFT_VIDEO_RES_TEXT,
        { 91, 315, 87, 13 },
    },

    // HWND_CHINFO_POP_FIRST_SRC_ICON
    {
        HWND_CHINFO_POP_LEFT_AREA, HWND_CHINFO_POP_FIRST_SRC_ICON,
        { 2, 261, 13, 9 },
    },

    // HWND_CHINFO_POP_RIGHT_AREA
    {
        HWND_CHINFO_POP_PANE, HWND_CHINFO_POP_SECOND_SRC_ICON,
        { 317, 291, 317, 66 },
    },

    // HWND_CHINFO_POP_RIGHT_TV_INFO
    {
        HWND_CHINFO_POP_RIGHT_AREA, HWND_CHINFO_POP_RIGHT_CH_AUDIO_TEXT,
        { 317, 255, 317, 102 },
    },

    // HWND_CHINFO_POP_RIGHT_TV_BG
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_TV_BG,
        { 317, 255, 317, 19 },
    },

    // HWND_CHINFO_POP_RIGHT_TV_BG_DOWN
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_TV_BG_DOWN,
        { 317, 276, 317, 82 },
    },

    // HWND_CHINFO_POP_RIGHT_TV_GREEN_BG
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_TV_GREEN_BG,
        { 317, 276, 317, 36 },
    },

    // HWND_CHINFO_POP_RIGHT_CH_NUMBER_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_CH_NUMBER_TEXT,
        { 341, 255, 87, 19 },
    },

    // HWND_CHINFO_POP_RIGHT_CH_NAME_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_CH_NAME_TEXT,
        { 434, 255, 87, 19 },
    },

    // HWND_CHINFO_POP_RIGHT_TIME_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_TIME_TEXT,
        { 527, 255, 87, 19 },
    },

    // HWND_CHINFO_POP_RIGHT_LARROW_ICON
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_LARROW_ICON,
        { 320, 283, 17, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_RARROW_ICON
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_RARROW_ICON,
        { 618, 283, 17, 18 },
    },

    // HWND_CHINFO_POP_RIGHT_TIME_START_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_TIME_START_TEXT,
        { 341, 294, 43, 18 },
    },

    // HWND_CHINFO_POP_RIGHT_TIME_END_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_TIME_END_TEXT,
        { 385, 294, 43, 18 },
    },

    // HWND_CHINFO_POP_RIGHT_NAME_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_NAME_TEXT,
        { 341, 276, 102, 18 },
    },

    // HWND_CHINFO_POP_RIGHT_GENRE_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_GENRE_TEXT,
        { 463, 294, 48, 18 },
    },

    // HWND_CHINFO_POP_RIGHT_AGE_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_AGE_TEXT,
        { 507, 294, 28, 18 },
    },

    // HWND_CHINFO_POP_RIGHT_TV_VIDEO_RES_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_TV_VIDEO_RES_TEXT,
        { 463, 315, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_CH_COLOR_SYS_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_CH_COLOR_SYS_TEXT,
        { 341, 330, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_CH_SOUND_SYS_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_CH_SOUND_SYS_TEXT,
        { 412, 330, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_CH_INPUT_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_CH_INPUT_TEXT,
        { 482, 330, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_CH_SDHD_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_CH_SDHD_TEXT,
        { 551, 330, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_CH_MTS_TYPE_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_CH_MTS_TYPE_TEXT,
        { 341, 315, 116, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_TELETEXT_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_TELETEXT_TEXT,
        { 551, 315, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_AUDIO_LANG_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_AUDIO_LANG_TEXT,
        { 551, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_SUBTITLE_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_SUBTITLE_TEXT,
        { 481, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_NARRATION_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_NARRATION_TEXT,
        { 482, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_MHEG_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_MHEG_TEXT,
        { 412, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_CH_AUDIO_TEXT
    {
        HWND_CHINFO_POP_RIGHT_TV_INFO, HWND_CHINFO_POP_RIGHT_CH_AUDIO_TEXT,
        { 341, 343, 40, 15 },
    },

    // HWND_CHINFO_POP_RIGHT_SRC_INFO
    {
        HWND_CHINFO_POP_RIGHT_AREA, HWND_CHINFO_POP_RIGHT_VIDEO_RES_TEXT,
        { 317, 288, 317, 69 },
    },

    // HWND_CHINFO_POP_RIGHT_SRC_BG
    {
        HWND_CHINFO_POP_RIGHT_SRC_INFO, HWND_CHINFO_POP_RIGHT_SRC_BG,
        { 317, 255, 317, 19 },
    },

    // HWND_CHINFO_POP_RIGHT_SRC_BG_LINE
    {
        HWND_CHINFO_POP_RIGHT_SRC_INFO, HWND_CHINFO_POP_RIGHT_SRC_BG_LINE,
        { 317, 276, 317, 82 },
    },

    // HWND_CHINFO_POP_RIGHT_SOURCE_TYPE_TEXT
    {
        HWND_CHINFO_POP_RIGHT_SRC_INFO, HWND_CHINFO_POP_RIGHT_SOURCE_TYPE_TEXT,
        { 342, 315, 40, 13 },
    },

    // HWND_CHINFO_POP_RIGHT_VIDEO_RES_TEXT
    {
        HWND_CHINFO_POP_RIGHT_SRC_INFO, HWND_CHINFO_POP_RIGHT_VIDEO_RES_TEXT,
        { 412, 315, 87, 13 },
    },

    // HWND_CHINFO_POP_SECOND_SRC_ICON
    {
        HWND_CHINFO_POP_RIGHT_AREA, HWND_CHINFO_POP_SECOND_SRC_ICON,
        { 323, 261, 13, 9 },
    },

};
