////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2006-2010 MStar Semiconductor, Inc.
// All rights reserved.
//
// Unless otherwise stipulated in writing, any and all information contained
// herein regardless in any format shall remain the sole proprietary of
// MStar Semiconductor Inc. and be kept in strict confidence
// (''MStar Confidential Information'') by the recipient.
// Any unauthorized act including without limitation unauthorized disclosure,
// copying, use, reproduction, sale, distribution, modification, disassembling,
// reverse engineering and compiling of the contents of MStar Confidential
// Information is unlawful and strictly prohibited. MStar hereby reserves the
// rights to any and all damages, losses, costs and expenses resulting therefrom.
//
////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////
// This file is automatically generated by SkinTool [Version:0.2.3][Build:Dec 18 2014 17:25:00]
/////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////
// Window List (Position)

WINDOWPOSDATA _MP_TBLSEG _GUI_WindowPositionList_Zui_Install_Guide[] =
{
    // HWND_MAINFRAME
    {
        HWND_MAINFRAME, HWND_INSTALL_MAIN_TEXT_2,
        { 90, 81, 456, 196 },
    },

    // HWND_INSTALL_BACKGROUND
    {
        HWND_MAINFRAME, HWND_INSTALL_BACKGROUND_BAR4,
        { 90, 81, 456, 196 },
    },

    // HWND_INSTALL_BACKGROUND_TOP
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_TOP,
        { 90, 81, 456, 30 },
    },

    // HWND_INSTALL_BACKGROUND_L
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_L,
        { 90, 111, 2, 166 },
    },

    // HWND_INSTALL_BACKGROUND_C
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_C,
        { 94, 111, 448, 166 },
    },

    // HWND_INSTALL_BACKGROUND_R
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_R,
        { 543, 111, 2, 166 },
    },

    // HWND_INSTALL_BACKGROUND_OK_BTN
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_OK_BTN,
        { 315, 259, 25, 12 },
    },

    // HWND_INSTALL_BACKGROUND_BAR1
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_BAR1,
        { 181, 111, 0, 141 },
    },

    // HWND_INSTALL_BACKGROUND_BAR2
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_BAR2,
        { 272, 111, 0, 141 },
    },

    // HWND_INSTALL_BACKGROUND_BAR3
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_BAR3,
        { 363, 111, 0, 141 },
    },

    // HWND_INSTALL_BACKGROUND_BAR4
    {
        HWND_INSTALL_BACKGROUND, HWND_INSTALL_BACKGROUND_BAR4,
        { 455, 111, 0, 141 },
    },

    // HWND_INSTALL_MAIN_PAGE
    {
        HWND_MAINFRAME, HWND_INSTALL_MAIN_TEXT_2,
        { 90, 81, 456, 196 },
    },

    // HWND_INSTALL_MAIN_TITLE
    {
        HWND_INSTALL_MAIN_PAGE, HWND_INSTALL_MAIN_TITLE,
        { 179, 81, 276, 30 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID
    {
        HWND_INSTALL_MAIN_PAGE, HWND_INSTALL_MAIN_PAGE_GRID15,
        { 186, 108, 4, 4 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID1
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID1,
        { 91, 111, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID2
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID_LEFT_ARROW,
        { 91, 159, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID_LEFT_ARROW
    {
        HWND_INSTALL_MAIN_PAGE_GRID2, HWND_INSTALL_MAIN_PAGE_GRID_LEFT_ARROW,
        { 91, 177, 6, 6 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID3
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID3,
        { 91, 207, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID4
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID4,
        { 182, 111, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID5
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID5,
        { 182, 159, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID6
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID6,
        { 182, 207, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID7
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID_UP_ARROW,
        { 274, 111, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID_UP_ARROW
    {
        HWND_INSTALL_MAIN_PAGE_GRID7, HWND_INSTALL_MAIN_PAGE_GRID_UP_ARROW,
        { 315, 111, 6, 6 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID8
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID8,
        { 274, 159, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID9
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID_DOWN_ARROW,
        { 274, 207, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID_DOWN_ARROW
    {
        HWND_INSTALL_MAIN_PAGE_GRID9, HWND_INSTALL_MAIN_PAGE_GRID_DOWN_ARROW,
        { 315, 244, 6, 6 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID10
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID10,
        { 365, 111, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID11
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID11,
        { 365, 159, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID12
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID12,
        { 365, 207, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID13
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID13,
        { 456, 111, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID14
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID_RIGHT_ARROW,
        { 456, 159, 88, 45 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID_RIGHT_ARROW
    {
        HWND_INSTALL_MAIN_PAGE_GRID14, HWND_INSTALL_MAIN_PAGE_GRID_RIGHT_ARROW,
        { 539, 177, 6, 6 },
    },

    // HWND_INSTALL_MAIN_PAGE_GRID15
    {
        HWND_INSTALL_MAIN_PAGE_GRID, HWND_INSTALL_MAIN_PAGE_GRID15,
        { 456, 207, 88, 45 },
    },

    // HWND_INSTALL_MAIN_TEXT_1
    {
        HWND_INSTALL_MAIN_PAGE, HWND_INSTALL_MAIN_TEXT_1,
        { 185, 153, 233, 18 },
    },

    // HWND_INSTALL_MAIN_TEXT_2
    {
        HWND_INSTALL_MAIN_PAGE, HWND_INSTALL_MAIN_TEXT_2,
        { 185, 186, 233, 18 },
    },

};
